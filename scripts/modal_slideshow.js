var slideIndex = 1;
var categoryIndex = 0;
var category = '';

function plusSlides(n) {
  if(n===-1){
      category = $('.prev').attr('data-category');
  }else{
      category =$('.next').attr('data-category');
  }
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var slide_images = document.getElementsByClassName("mySlides-img");

  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
  }

  if (n > slide_images.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slide_images.length}
  for (i = 0; i < slide_images.length; i++) {
      slide_images[i].style.display = "none";
  }

  slides[slideIndex-1].style.display = "block";
  slide_images[slideIndex-1].style.display = "block";

  $(".modal-header h5" ).text($('.' + category + '-feature-text-' + slideIndex).attr('data-title'));
}

$(document).ready(function(){
  $('.feature-button').click(function(){
    slideIndex = 1;
  });
});
