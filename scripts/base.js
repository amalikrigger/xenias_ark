$(document).ready(function(){

  // Open Side Bar
  $(".navbar-toggler").click(function(e){
      var menu = $("#sidebar");
      if ($(menu).is(":visible")) {
          $(menu).animate({left: -350}, 500, function() {$(menu).hide();});
      } else {
          $(menu).show().animate({left: 0}, 500);
      }
  });

  // Spin Scroll Icon
  var winHeight = $( window ).height();

  $( window ).scroll( function() {
    $('.scrollIcon').css("display", "grid");
    var scrTop = $( document ).scrollTop() / winHeight,
    scrTopFixed = scrTop.toFixed(2),
    scrTransform = scrTopFixed * 360;

      /*$('.scrollIcon').css({
        '-webkit-transform': "rotateZ(" + scrTransform + "deg)",
        '-moz-transform': "rotateZ(" + scrTransform + "deg)",
        'transform': "rotateZ(" + scrTransform + "deg)",
      });*/

    TweenMax.to(".scrollIcon", 1, {rotation: scrTransform});

    });

  // $(window).scroll($.debounce( 250, function(){
  //   $('.scrollIcon').hide();
  // }));

  // Add feature button (Read More) functionality
  $('.feature-button').click(function() {
    var id = $(this).attr('id');
    var index = id.substring(id.indexOf('-') + 1);
    count = $('#category-' + index + ' p').length;
    var text = '';
    var headerText = $('#category-' + index + ' h5').text();

    $( ".modal-body" ).empty();
    $( ".modal-footer" ).empty();
    $( ".modal-header h5" ).empty();
    //$(".modal-footer").css("display", "flex");

    if ($('#category-' + index).hasClass('modal-slide')){
      $(".modal-body").append('<div class="slideshow-container"></div>');
      $(".modal-footer").append('<div class="slideshow-container"><div class="directional-wrapper"><a class="prev" data-category="category-'+ index +'" onclick="plusSlides(-1)">&#10094;</a></div>');
      for(var i = 1; i <= count; i++) {
        text = $('.category-' + index + '-feature-text-' + i).text();
        $(".modal-body .slideshow-container").append('<div class="mySlides"><p>' + text + '</p></div>');
        $(".modal-footer .slideshow-container").append('<div class="mySlides-img"><img class="img-fluid rounded" src="img/Category-' + index + '-Image-'+ i +'.png" alt="-"></div>');
      }

      $(".modal-footer .slideshow-container").append('<div class="directional-wrapper"><a class="next" data-category="category-'+ index +'" onclick="plusSlides(1)">&#10095;</a></div>');

      $(".mySlides").toggle();
      $(".mySlides-img").toggle();
      $(".mySlides").first().toggle();
      $(".mySlides-img").first().toggle();

    } else {
      for(var i = 1; i <= count; i++) {
        text = $('.category-' + index + '-feature-text-' + i).text();
        $(".modal-body").append('<p>' + text + '</p>');
      }

      $.ajax({
          url:'img/Category-' + index + '-Image.png',
          type:'HEAD',
          error: function()
          {
              if (!$('#category-' + index).hasClass('modal-slide')) {
                $(".modal-footer").css("display", "none");
              }
          },
          success: function()
          {
            $(".modal-footer").append('<img class="img-fluid rounded" src="img/Category-' + index + '-Image.png" alt="-">');
          }
      });
    }

    if ($('#category-' + index).hasClass('modal-slide')){
      $(".modal-header h5" ).text($('.category-' + index + '-feature-text-1').attr('data-title'));
    } else {
      $(".modal-header h5" ).text(headerText);
    }

  });

  $('.close').click(function(){
    $('.feature-button').removeClass('hvr-pulse');
  });

   $(".feature-button").hover(function() {
      $(this).addClass('hvr-pulse');
   }, function() {
      $(this).removeClass('hvr-pulse');
   });

  // Shut off nav bar links
  $('.inactive').click(function(){
    alert('Section coming soon!');
    return false;
  });

  // Toggle Nav Bar Dropdown
  $('.dropdown-toggle-screen').click(function() {
    $('.dropdown-menu-screen').toggle("slow");
  });

  $('.dropdown-toggle-mobile').click(function() {
    $('.dropdown-menu-mobile').toggle("slow");
  });

  $('.dropdown-item-screen').click(function() {
    $('.dropdown-menu-screen').toggle("slow");
  });

  $('.dropdown-item-mobile').click(function() {
    $('.dropdown-menu-mobile').toggle("slow");
    $('#sidebar').toggle("slow");
  });

  // Take off dropdown menu automatic highlight
  $('.nav-link').click(function() {
    var timer = setInterval(function(){ $('.dropdown-item').removeClass('active'); }, 3);
    setInterval(function(){ clearInterval(timer);; }, 3000);
  });

});


function topFunction() {
  TweenMax.to("html, body", 2, {scrollTop: 0, ease: Power1.easeOut});
}

$(document).on("scroll", function () {

  var pageTop = $(document).scrollTop();

  if(pageTop > $(window).height() - 300){
    $('.scrollIcon').css('z-index', 2);
  }else if(pageTop < $(window).height()){
    $('.scrollIcon').css('z-index', 0);
  }
})
